package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class startServer {
	public startServer() {
	}
	public startServer(int port){
		try {
			ServerSocket serverSocket = new ServerSocket(port);
			while (true) {
				Socket socket = serverSocket.accept();
				new myProcess(socket).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		int port = 8080;
		new startServer(port);
	}
}
